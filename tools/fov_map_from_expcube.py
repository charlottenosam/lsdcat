#!/usr/bin/env python
##
# FILE: fov_map_from_expcube.py
# AUTHOR: C. Herenz (2015)
# DESCR.: Create map of number of exposed voxels within FoV
#         (part of LSDCat suite)

__version__ = '1.0.2'

import argparse
import numpy as np
from astropy.io import fits

parser = argparse.ArgumentParser(description="""
Creates from an exposure map datacube an exposure map image.  An exposure map datacube contains in every voxel the number of exposures that went into this voxel, while an exposure map image contains the number of exposure for each spatial pixel.  Such a map can be used, e.g., to identify regions without any exposures (e.g. field borders).
""")
parser.add_argument('input',type=str,help="""
Input FITS File""")
parser.add_argument('-E','--exphdu',type=int,default=3,help="""
FITS HDU containing the exposure cube (0-indexed). Default: 3""")

args = parser.parse_args()
inputfile = args.input
exp_hdu_num = args.exphdu
outfile = inputfile[:inputfile.find('.fits')]+'_expmap.fits'

print('fov_map_from_expcube.py - Reading Exp.-Cube from '+inputfile+\
      ' (HDU '+str(exp_hdu_num)+')')
hdu = fits.open(inputfile)
exp_cube = hdu[exp_hdu_num].data
exp_cube_header = hdu[exp_hdu_num].header
exp_cube_header['FOVIN'] = inputfile
exp_cube_header['FOVINH'] = (exp_hdu_num,'0-indexed')

print('fov_map_from_expcube.py - Summing up all spaxels...')
exp_map = exp_cube.sum(axis=0)

print('fov_map_from_expcube.py - ... Done! Saving output '+outfile)
fits.writeto(outfile, exp_map, header=exp_cube_header)
print('fov_map_from_expcube.py - All done!')
